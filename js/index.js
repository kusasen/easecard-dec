$(function () {
  var gaCode = "UA-175302189-1";
  
  $("#load-imgs")
    .imagesLoaded()
    .always(function (e) {
      if (e.isComplete) {
        TweenMax.to("#loading", 0.8, {
          autoAlpha: 0,
          display: "none",
          ease: Linear.easeNone,
        });

        init.play();
      }
    });

  
  var init = new TimelineMax();
  init
    .from(".kv-slogan", 0.8, {
      scale: 0.2,
      ease: Back.easeOut,
      autoAlpha: 0,
    })
    .from(
      ".kv-bebe",
      1,
      {
        y: -20,
        scale: 0.98,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=.8"
    ).from(
      ".kv-money1",
      1,
      {
        y: -20,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=.8"
    ).from(
      ".kv-money8",
      1,
      {
        y: -20,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=.8"
    ).from(
      ".kv-money2",
      1,
      {
        y: -20,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=.8"
    ).from(
      ".kv-money3",
      1,
      {
        y: -20,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=.8"
    ).from(
      ".kv-money4",
      1,
      {
        y: -20,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=.8"
    ).from(
      ".kv-money5",
      1,
      {
        y: -20,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=.8"
    ).from(
      ".kv-money6",
      1,
      {
        y: -20,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=.8"
    );

  init.pause();

  var settting = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: false,
    speed: 600,
    dots: false,
    autoplay: true,
    autoplaySpeed: 8000,
  };


  // slide change
  $(".slide-slides").slick(settting);
  

  $(".back-top").click(function (e) {
    e.preventDefault();
    $("body,html").animate({
      scrollTop: 0,
    });
  });

  var $navBtn = $(".btn-menu");
  var $navCloseBtn = $(".btn-menu-close");
  var $navBox = $(".menu");
  var $overlay = $(".menu-overlay");
  //navigation
  $navBtn.click(function (e) {
    e.preventDefault();
    $("html").addClass("lock-body");
    $navBox.addClass("animation");
    TweenMax.to($navBox, 0.8, {
      autoAlpha: 1,
      display: "block",
      ease: Quart.easeOut,
    });
    TweenMax.to($overlay, 0.8, {
      autoAlpha: 1,
      display: "block",
      ease: Quart.easeOut,
    });
  });

  $navCloseBtn.click(function (e) {
    e.preventDefault();
    $("html").removeClass("lock-body");
    $navBox.removeClass("animation");
      TweenMax.to($navBox , 0.8, {
        autoAlpha: 0,
        display: "none",
        ease: Quart.easeOut,
      });
      TweenMax.to($overlay, 0.8, {
        autoAlpha: 0,
        display: "none",
        ease: Quart.easeOut,
      });
  });


  $(".linkTo").click(function () {
    var getSection = $(this).attr("href");
    $navBtn.removeClass("active");
    $navBox.removeClass("animation");
    TweenMax.to($navBox , 0.8, {
      autoAlpha: 0,
      display: "none",
      ease: Quart.easeOut,
    });
    TweenMax.to($overlay, 0.8, {
      autoAlpha: 0,
      display: "none",
      ease: Quart.easeOut,
    });
  });

  //autoplay
  $(".slide-slides")
    .on("beforeChange", function (event, slick, currentSlide, nextSlide) {
      $(this).parent().find(".slide-navis").find("a.active").removeClass("active");
      $(this).parent().find(".slide-navis").find("a").eq(nextSlide).addClass("active");
    })
    .on("afterChange", function (event, slick, currentSlide, nextSlide) {
      let getGa = $(this).find(".item").eq(currentSlide+1).find(".slide-group").attr("data-pg-ga") ? $(this).find(".item").eq(currentSlide+1).find(".slide-group").attr("data-pg-ga"): false;

      if(getGa){
        console.log("page:"+getGa);
        gtag("config", gaCode, { page_title: getGa });
      }
    });
    
  $(".slide-navis a").click(function (e) {
    e.preventDefault();
    var index = $(this).index();
    $(this).parent().parent().find(".slide-slides").slick("slickGoTo", index);
  });




  // Instantiate new modal
  var event1_1 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event1-1",
    },
  });

  var event1_2 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event1-2",
    },
  });

  var event2_1 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event2-1",
    },
  });

  var event2_2 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event2-2",
    },
  });

  var event2_3 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event2-3",
    },
  });

  var event3_1 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event3-1",
    },
  });

  var event3_2 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event3-2",
    },
  });

  var event3_3 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event3-3",
    },
  });

  var event3_4 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event3-4",
    },
  });

  var event4_1 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event4-1",
    },
  });

  var event4_2 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event4-2",
    },
  });

  var event4_3 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event4-3",
    },
  });

  var event4_4 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event4-4",
    },
  });

  var event5 = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#event5",
    },
  });


  $(".btn.event1-1").click(function (e) {
    e.preventDefault();
    event1_1.open();
  });

  $(".btn.event1-2").click(function (e) {
    e.preventDefault();
    event1_2.open();
  });

  $(".btn.event2-1").click(function (e) {
    e.preventDefault();
    event2_1.open();
  });

  $(".btn.event2-2").click(function (e) {
    e.preventDefault();
    event2_2.open();
  });

  $(".btn.event2-3").click(function (e) {
    e.preventDefault();
    event2_3.open();
  });

  $(".btn.event3-1").click(function (e) {
    e.preventDefault();
    event3_1.open();
  });

  $(".btn.event3-2").click(function (e) {
    e.preventDefault();
    event3_2.open();
  });

  $(".btn.event3-3").click(function (e) {
    e.preventDefault();
    event3_3.open();
  });

  $(".btn.event3-4").click(function (e) {
    e.preventDefault();
    event3_4.open();
  });

  $(".btn.event4-1").click(function (e) {
    e.preventDefault();
    event4_1.open();
  });

  $(".btn.event4-2").click(function (e) {
    e.preventDefault();
    event4_2.open();
  });

  $(".btn.event4-3").click(function (e) {
    e.preventDefault();
    event4_3.open();
  });

  $(".btn.event4-4").click(function (e) {
    e.preventDefault();
    event4_4.open();
  });

  $(".btn.event5").click(function (e) {
    e.preventDefault();
    event5.open();
  });

});
