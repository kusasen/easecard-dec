$(document).ready(function () {
  $("a").click(function (e) {
    var BtnCode = $(this).attr("data-ga");

    if (BtnCode != undefined) {
      trackBtn(BtnCode);
      console.log(BtnCode);
    }
  });

  function trackBtn(BtnCode) {
    gtag("event", BtnCode, {
      event_category: "click",
    });
  }
});
